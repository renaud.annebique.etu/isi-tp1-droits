# Vérification lecture fichier et sous-répertoire dans dir_b

cd dir_b
cat lambda_b.txt
read
cd dir_dir_b
read
cd ../..


# Vérification lecture fichier dans dir_c mais pas de modification/création autorisé

cd dir_c
cat lambda_c.txt
read
touch test_b.txt
read
mv lambda_c.txt move_b.txt
read
cd ..

# Vérification modification/création autorisé dans dir_b

cd dir_b
touch test_b.txt
read
mv test_b.txt move_b.txt
read
unlink move_b.txt
cd ..

# Vérification suppression/modification des fichiers dont il n'est pas propriétaire

cd dir_b
mv test_root.txt move_b.txt
read
unlink test_root.txt
read
cd ..

#Verification dir_a

cd dir_a
read
touch dir_a/test_b.txt
read
cd ..
