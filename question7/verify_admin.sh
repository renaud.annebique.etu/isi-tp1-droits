# Vérification lecture fichier et sous-répertoire dans dir_a

cd dir_a
cat lambda_a.txt
read
cd dir_dir_a
read
cd ../..


# Vérification lecture fichier dans dir_c

cd dir_c
cat lambda_c.txt
read
touch test_admin.txt
read
mv test_admin.txt move_admin.txt
read
cd ..

# Vérification modification/création autorisé dans dir_a

cd dir_a
touch test_a.txt
read
mv test_a.txt move_a.txt
read
unlink move_a.txt
cd ..

# Vérification suppression/modification des fichiers dont il n'est pas propriétaire

cd dir_a
mv test_root.txt move_a.txt
read
unlink test_root.txt
read
cd ..

#Verification dir_b

cd dir_b
read
touch dir_b/test_a.txt
read
cd ..

#Verification suppresion dans dir_a,dir_b et dir_c

cd dir_a
touch test_admin.txt
read
unlink test_admin.txt
cd ../dir_b
touch test_admin.txt
read
unlink test_admin.txt
cd ../dir_c
touch test_admin.txt
unlink test_admin.txt