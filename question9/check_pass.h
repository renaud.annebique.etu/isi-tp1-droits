int check_username(char *username);
int check_password(char *password, char *username);
char *generateCryptedPassword(char *password);
int change_password(char *password, char *username, char *option);