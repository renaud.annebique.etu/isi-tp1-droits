#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include <time.h>

int check_username
(char *username){

    FILE *fptr;

    fptr = fopen("/home/administrateur/passwd", "r");

    if (fptr == NULL){
        printf("Cannot open file \n");
        exit(0);
    }

    char line[256];
    
    while (fgets(line, sizeof(line),fptr)){
        if(strstr(line,username)){
            fclose(fptr);
            return 0;
        }
    }

    fclose(fptr);
    return 1;
}

int check_password
(char *password, char *username){
    
    FILE *fptr;

    fptr = fopen("/home/administrateur/passwd", "r");

    if (fptr == NULL){
        printf("Cannot open file \n");
        exit(0);
    }

    char line[256];
    char *pass = strcat(password, "\n");

    while (fgets(line, sizeof(line),fptr)){
        if(strstr(line,username)){
            if (strstr(line,pass)){
                fclose(fptr);
                return 0;
            }
        }
    }

    fclose(fptr);
    return 1;

}

char *generateCryptedPassword
(char *password){

    unsigned long seed[2];
    char salt[] = "$1$........";
    const char *const seedchars = 
        "./0123456789ABCDEFGHIJKLMNOPQRST"
        "UVWXYZabcdefghijklmnopqrstuvwxyz";
    char *res;
    int i;

    seed[0] = time(NULL);
    seed[1] = getpid() ^ (seed[0] >> 14 & 0x30000);
    
    for (i = 0; i < 8; i++)
        salt[3+i] = seedchars[(seed[i/5] >> (i%5)*6) & 0x3f];

    res = crypt(password,salt);

    return res;
}

int change_password
(char *password, char *username, char *option){
    FILE *fptr;

    fptr = fopen("/home/administrateur/passwd", "w");

    if (fptr == NULL){
        printf("Cannot open file \n");
        exit(0);
    }

    if (strcmp(option,"NEW")){
        char *newUser;
        newUser = strcat(username,(strcat(",",generateCryptedPassword(password))));
        fprintf(fptr,"%s",newUser);
    }

    else{
        
    }
}
