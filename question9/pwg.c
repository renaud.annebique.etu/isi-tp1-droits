#include<stdio.h>
#include "check_pass.h"

int main (int argc, char * argv[]){
    char username[80];
    char password[80];
    char newPassword[80];

    printf("Please enter your username");
    scanf("%s",username);

    int alreadyExist = check_username(username);

    if (alreadyExist == 0){
        printf("Please enter your password");
        scanf("%s",password);

        int autorisation = check_password(password,username);

        if (autorisation == 0){

            printf("Please enter your new password");
            scanf("%s",newPassword);

        }
        else{
            printf("Your password or username is incorrect");
        }
    }

    else{
        printf("Please choose a password");
        scanf("%s",newPassword);
        change_password(newPassword,username,"NEW");
    }
}