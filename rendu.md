# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- ANNEBIQUE, Renaud, renaud.annebique.etu@univ-lille.fr


## Question 1

Après avoir essayé en reproduisant le fichier et ces permissions, toto ne peut pas enregister de modifications sur le fichier "myfile.txt".

## Question 2

- Le caractère x pour un repetoire signifie qu'il est possible d'ouvrir ce répertoire pour les utilisateurs/groupes qui en ont la permission
- Le message "Permission denied" est retourné puisque le groupe ubuntu n'a plus accès à l'éxécution de ce dossier et vu que toto fait parti du groupe ubuntu il n'y a plus accès non plus.
- Des points d'interrogations apparaisent à la place des permissions d'accès sur le fichier data.txt car toto n'a plus accès à l'éxécution sur le dossier mydir.

## Question 3

- Les valeurs des différentes id sont toutes égales à 1001 et le processus n'arrive pas à ouvrir le fichier mydata.txt en lecture.

- Les valeurs des différentes id sont toutes égales à 1001 sauf le RUID qui est égal à 1000 et le fichier s'ouvre correctement cette fois ci.

## Question 4

- Les valeurs de EUID et EIGD sont 1001 pour toto

## Question 5

La commande "chfn" est utilisée pour changer le nom d'un utilisateur et certaines informations (numéro de téléphone maison, numéro de téléphone bureau et numéro de salle au bureau).

La commande ls -al /usr/bin/chfn produit le résultat suivant : 
	-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn

Nous savons donc que root à les permissions de lecture/écriture/execution avec set-user-id et que le groupe root et tous les utilisateurs ont la permission en lecture et en exécution.

Les informations sont bien mis à jour dans le fichier passwd si toto utilise la commande chfn.

## Question 6

Les mots de passe sont stockés dans /etc/shadow et ils sont chiffrés pour une question de sécurité.

## Question 7

Pour cette question, différents utilisateurs, groupes et dossiers ont été crées :

	- Un utilisateur lambda_a et un utilisateur lambda_a2
	- Un utilisateur lambda_b et un utilisateur lambda_b2
	- Un groupe "group_a" et un groupe "group_b"
	- Un utilisateur "administrateur"
	- Un dossier dir_a créer par l'administrateur dont le groupe propriétaire est "group_a"
	- Un dossier dir_b créer par l'administrateur dont le groupe propriétaire est "group_b"
	- Un dossier dir_c créer par l'administrateur dont le groupe propriétaire est "administrateur"

Les différents dossiers ont les permissions suivantes :

drwxrws--T 4 administrateur groupe_a       4096 Jan 23 20:27 dir_a

drwxrws--T 2 administrateur groupe_b       4096 Jan 23 19:39 dir_b

drwx---r-x 2 administrateur administrateur 4096 Jan 26 00:33 dir_c

Les scripts ont les permissions suivantes :

-rw-rwxr-- 1 root           groupe_a        683 Jan 26 22:14 verify_a.sh

-rw-rwxr-- 1 root           administrateur  875 Jan 26 22:42 verify_admin.sh

-rw-rwxr-- 1 root           groupe_b        684 Jan 26 22:42 verify_b.sh


## Question 8

Le programme et les scripts dans le repertoire *question8*.

Utiliser la commande "make" pour générer le programme rmg, pour utiliser ce programme taper ./rmg suivit d'un nom de fichier que vous souhaitez supprimer

Le fichier check_pass.c permet de vérifier que le mot de passe entrée par l'utilisateur est bien dans la "base de donnée"

Le fichier check_pass.h contient la déclaration de la méthode incluse dans le fichier check_pass.c

Le fichier rmg.c contient la fonction de suppression, cette fonction demande un nom de fichier à supprimer en argument et le fichier ne peut être supprimé que si :

	- Il y a bien un argument passé en paramètre
	- Il existe
	- Le mot de passe fournit par l'utilisateur est bien dans la base de donnée
	- L'utilisateur a bien la permission de supprimer ce fichier

Ne pas oublier de changer le droit d'éxecution du programme afin que tous les utilisateurs du système puissent l'utiliser (en faisant chmod o+x sur le fichier rmg genéré par le makefile) 

## Question 9

Le programme et les scripts dans le repertoire *question9*.

Le programme pwg demande à l'utilisateur de rentrer son nom puis il vérifie si il existe déja, si il existe déja il lui demande d'entrer son mot de passe actuel afin de le changer, sinon il lui demande de rentrer un mot de passe afin de "créer" un compte dans 
la base de donnée.

## Question 10

Question non traitée







