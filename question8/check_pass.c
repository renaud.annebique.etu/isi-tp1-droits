#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>

int checkpass
(char *password,char *username){

    FILE *fptr;

    fptr = fopen("/home/administrateur/passwd", "r");

    if (fptr == NULL){
        printf("Cannot open file \n");
        exit(0);
    }

    char line[256];
    char *pass = strcat(password, "\n");

    while (fgets(line, sizeof(line),fptr)){
        if(strstr(line,username)){
            if (strstr(line,pass)){
                fclose(fptr);
                return 0;
            }
        }
    }

    fclose(fptr);
    return 1;
}
