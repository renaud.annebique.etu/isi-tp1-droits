#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include"check_pass.h"

int main (int argc, char * argv[]){
        if (argc < 2){
            printf("Please enter the name of a file you want to delete\n");
            return 0;
        }

        FILE *file;

        file = fopen(argv[1],"r");

        if (file == NULL){
                printf("File does not exist and therefore can't be deleted\n");
                return 0;
        }

        if (getuid() == getgid()){
                char password[80];
                char username[80];

                printf("Please enter your username\n");
                scanf("%s",username);

                printf("Please enter your password\n");
                scanf("%s", password);

                int autorisation = checkpass(password,username);

                if (autorisation == 0){
                        remove(argv[1]);
                        printf("The file %s was deleted\n", argv[1]);
                }
                else{
                        printf("The password and/or the username you entered is incorrect\n");
                }
        }

        else{
                printf("You do not have the permission to delete this file\n");
        }

        return 0;

}
