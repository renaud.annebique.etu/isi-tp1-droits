import os

euid = os.geteuid()
egid = os.getegid()

print("EUID: ", euid)
print("EGID: ", egid)